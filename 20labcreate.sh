#!/bin/bash
# vim: tabstop=2 shiftwidth=2 expandtab

# Copyright © 2014,2015,2016,2017,2018,2019,2020,2021 Alessandro Ugo, Marco Sciuto, Filippo Bonazzi, Marco De Benedictis, Carmelo Riolo, Giuseppe D'Andrea, Massimo Gismondi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>


################################################################################
######################## Controllo dei permessi di root ########################
################################################################################

if ! [ $(id -u) = 0 ]; then
  echo "Sono necessari i permessi di root!"
  exit 1
fi

################################################################################
############################ Variabili dello script ############################
################################################################################

# Cartella generale openscuola, con sottocartelle script e modelli
openscuola_main_dir="/opt/openscuola"
# Cartella con i soli script OpenScuola
script_dir="${openscuola_main_dir}/lab-configuration-scripts"
# Cartella modelli Libreoffice
libreoffice_dir="${openscuola_main_dir}/modelli-libreoffice"
# Cartella configurazione. Qui verranno salvati i file di configurazione laboratorio
config_dir="${openscuola_main_dir}/config"

# File da importare contenente le variabili di configurazione del laboratorio
config_file="${config_dir}/labconfig.sh"
# URL da dove scaricare l'elenco codice_PC->MAC
pcmac_url="https://linux.studenti.polito.it/wp/wp-content/plugins/openscuola/get_data.php?query=getMacs"
# File dove salvare l'elenco codice_PC->MAC del laboratorio
pcmac_file="${config_dir}/pcmac.txt"
# File di configurazione di Netplan
netplan_conf="/etc/netplan/config.yaml"
# File di configurazione di dnsmasq per il server DHCP di LTSP
ltsp_dnsmasq_conf="/etc/dnsmasq.d/ltsp-server-dnsmasq.conf"
# File dei leases di dnsmasq per il server DHCP di LTSP
ltsp_dnsmasq_leases="/var/lib/misc/dnsmasq.leases"
# File di configurazione dell'ambiente LTSP
ltsp_conf="/var/lib/tftpboot/ltsp/i386/lts.conf"
# File di configurazione di syslog-ng
ltsp_syslog_conf="/etc/syslog-ng/conf.d/ltsp.conf"
# File di configurazione di lightdm
lightdm_conf="/etc/lightdm/lightdm.conf.d/lightdm.conf"
# File di configurazione di slick-greeter
slick_greeter_conf="/etc/lightdm/slick-greeter.conf"
# Cartella di Xsession
xsession_dir="/usr/share/xsessions"
# File .desktop della sessione di default
default_session_desktop="${xsession_dir}/mate.desktop"
# Cartella di Wayland
wayland_dir="/usr/share/wayland-sessions"
# Cartella di configurazione di AccountsService
accounts_service_dir="/var/lib/AccountsService/users"
# File di configurazione del server CUPS
cups_server_conf="/etc/cups/cupsd.conf"
# Cartella di configurazione di Polkit
polkit_dir="/etc/polkit-1/rules.d"
# File di configurazione di Polkit
polkit_conf="${polkit_dir}/settings.rules"
# File di configurazione di nm-applet
nm_applet_conf="/etc/xdg/autostart/nm-applet.desktop"
# Cartella di configurazione di dconf
dconf_dir="/etc/dconf"
# Nome del db di dconf
dconf_db="mate"
# File di configurazione di VNC
vnc_conf="/lib/systemd/system/x11vnc.service"
# File di configurazione password VNC
vnc_pw_conf="/etc/x11vnc.pass"
# File di configurazione sysctl
sysctl_conf="/etc/sysctl.d/60-openscuola.conf"

################################################################################
################# Variabili di configurazione del laboratorio ##################
################################################################################

# Controllo se il file delle variabili di configurazione esiste
if ! [ -f "${config_file}" ]; then
  echo "Il file ${config_file} non esiste!"
  exit 1
fi

# Controllo se il file delle variabili di configurazione del laboratorio contiene solo commenti o righe del tipo Variabile=Valore
if grep -Eqv "^\s*$|^\s*#|^\s*[^ ]*=\"[^;]*\"$" "${config_file}"; then
  echo "Il file ${config_file} non e' corretto!"
  exit 1
fi

# Importa le variabili di configurazione del laboratorio
source "${config_file}"

################################################################################
####################### Gestione parametri dello script ########################
################################################################################

while getopts "r" parametro
do
  case "${parametro}" in
    r)
      echo "Ripristino..."

      # Controllo account corrente
      echo "Per ripristinare e' necessario che l'utente \"${username_docente}\" e gli utenti \"${username_client_base}XX\" non siano loggati."
      read -r -p "Proseguire? [s/N]: " risposta
      case "${risposta}" in
        [sS][iI]|[sS])
          ;;

        *)
          echo "Esecuzione script terminata!"
          exit 1
          ;;
      esac

      # TODO cancellare file con elenco PC->MAC?

      # Cancellazione file di configurazione di Netplan
      rm -rf "${netplan_conf}"
      netplan apply
      /etc/init.d/network-manager restart

      # Reset file di configurazione di dnsmasq per il server DHCP di LTSP
      /etc/init.d/dnsmasq stop
      ltsp-config dnsmasq --no-proxy-dhcp --overwrite
      /etc/init.d/dnsmasq start

      # Reset file dei leases di dnsmasq per il server DHCP di LTSP
      > "${ltsp_dnsmasq_leases}"

      # Cancellazione utente 'docente'
      usermod -G "" "${username_docente}" # Rimuozione 'docente' dai gruppi appartenenti
      userdel -rf "${username_docente}"

      # Cancellazione utenti 'studenteXX'
      for i in $(seq ${n_utenti}); do
        username_client="${username_client_base}$(printf "%02d" ${i})"
        usermod -G "" "${username_client}" # Rimuozione 'studenteXX' dai gruppi appartenenti
        userdel -rf "${username_client}"
      done

      # Cancellazione gruppi docente e studente
      gruppo_docente=$(echo -n ${nome_lab} | tr -cd '[:alpha:]' | tr '[:upper:]' '[:lower:]')
      groupdel "${gruppo_docente}"
      groupdel "${username_client_base}"

      # Reset file di configurazione dell'ambiente LTSP
      ltsp-config lts.conf --overwrite

      # Cancellazione file di configurazione di syslog-ng
      rm -rf "${ltsp_syslog_conf}"
      /etc/init.d/syslog-ng restart

      # Cancellazione file di configurazione di lightdm
      rm -rf "${lightdm_conf}"
      rm -rf "${slick_greeter_conf}"

      # Reset Xsession e Wayland
      for backup_file in ${xsession_dir}/*.bak ${wayland_dir}/*.bak; do
        file=$(echo -n "${backup_file}" | sed -r 's/.bak//')
        mv "${backup_file}" "${file}"
      done

      # Reset AccountsService
      sed -i "s/SystemAccount=true/SystemAccount=false/" "${accounts_service_dir}/${username_admin}"

      # Cancellazione file di configurazione di Polkit
      rm -rf "${polkit_conf}"

      # Reset di nm-applet
      mv "${nm_applet_conf}.bak" "${nm_applet_conf}"

      # Reset di cups
      mv "${cups_server_conf}.bak" "${cups_server_conf}"
      systemctl restart cups

      # Reset di dconf
      rm -rf "${dconf_dir}/db/${dconf_db}*"
      rm -rf "${dconf_dir}/profile/user"
      dconf update

      # Cancellazione file di configurazione di VNC
      rm -rf "${vnc_conf}"
      rm -rf "${vnc_pw_conf}"

      # Cancellazione file di configurazione di sysctl
      rm -f "${sysctl_conf}"

      # Riavvio del sistema
      echo -e "\nE' necessario riavviare il sistema per rendere effettivo il ripristino."
      read -r -p "Eseguire il riavvio? [S/n]: " risposta
      case "${risposta}" in
        [nN][oO]|[nN])
          echo "Eseguire il riavvio manualmente!"
          ;;

        *)
          reboot
          ;;
      esac

      exit 0
      ;;

    [?])
      echo "Parametro non riconosciuto!"
      exit 1
      ;;
  esac
done

################################################################################
###################### Verifica variabili configurazioni #######################
################################################################################

echo  "Attuali variabili di configurazione:
- Nome scuola: ${nome_lab}
- Nome utente dei client: ${username_client_base}
- Nome utente del docente: ${username_docente}
- Elenco client: ${macchine}
- Elenco client X: ${XMAC}
- Numero utenti da creare: ${n_utenti}
- Parametri rete del laboratorio:
-- MAC delle interfacce di rete: ${iface_lab_mac}
-- IP del server LTSP: ${address_lab}
-- Netmask: ${netmask_lab}
-- IP iniziale dei client: ${dhcp_range_begin}
-- IP finale dei client: ${dhcp_range_end}
-Parametri rete esterna:
-- Tipo di rete: ${ip_type_out}
-- MAC dell'interfaccia di rete: ${iface_out_mac}
-- IP del server nella rete esterna: ${address_out}
-- Netmask: ${netmask_out}
-- Gateway: ${gateway_out}
-- Server DNS: ${dns_nameservers_out}
"
read -r -p "Proseguire? [s/N]: " risposta
case "${risposta}" in
  [sS][iI]|[sS])
    ;;

  *)
    echo "Esecuzione script terminata!"
    exit 1
    ;;
esac

################################################################################
################### Configurazione delle interfacce di rete ####################
################################################################################

cat > "${netplan_conf}" << EOF
network:
  version: 2
  renderer: networkd
  ethernets:
EOF

# --- scheda lato internet ---
cat >> "${netplan_conf}" << EOF
    iface_out:
      match:
        macaddress: ${iface_out_mac}
EOF

if [ "${ip_type_out}" = "DHCP" ]; then
  cat >> "${netplan_conf}" << EOF
      dhcp4: true
      nameservers:
        addresses: [${dns_nameservers_out}]
EOF
else
  cat >> "${netplan_conf}" << EOF
      addresses: [${address_out}${netmask_out}]
      gateway4: ${gateway_out}
      nameservers:
        addresses: [${dns_nameservers_out}]
EOF
fi

# --- scheda/e lato laboratorio ---
#  creazione vettore contente i mac delle schede
iface_lab_macs=( ${iface_lab_mac//,/ } )
# controlla se abbiamo piu` di una scheda di rete lato laboratorio
if [ ${#iface_lab_macs[@]} -gt 1 ]; then
  # creazione del gruppo di schede da usare in bonding
  let i=1
  for mac in ${iface_lab_macs[@]}; do
    cat >> "${netplan_conf}" << EOF
    lab_nic${i}:
      match:
        macaddress: ${mac}
EOF
    let i++
  done
  # creazione del bonding
  cat >> "${netplan_conf}" << EOF
  bonds:
    iface_lab:
EOF
  printf '      interfaces: [' >> "${netplan_conf}"
  let i=0
  while [ $i -lt $((${#iface_lab_macs[@]} - 1)) ]; do
    let i++
    printf "lab_nic${i}, " >> "${netplan_conf}"
  done
  let i++
  echo "lab_nic${i}]" >> "${netplan_conf}" 
  cat >> "${netplan_conf}" << EOF
      addresses: [${address_lab}${netmask_lab}]
      parameters:
        mode: 802.3ad
        mii-monitor-interval: 1
EOF
else
  # solo un'interfaccia di rete lato lab
  cat >> "${netplan_conf}" << EOF
    iface_lab:
      match:
        macaddress: ${iface_lab_mac}
      addresses: [${address_lab}${netmask_lab}]
EOF
fi

netplan apply # Per rendere effettive le modifiche fatte

/etc/init.d/network-manager restart # Riavvio del Network Manager per avvisarlo di non gestire più le interfacce configurate precedentemente

################################################################################
####################### MAC dei client del laboratorio #########################
################################################################################

# Scaricamento dell'elenco di tutti i mac con i relativi codici dei PC
# utile se la rete non e` ancora configurata
sleep 3
wget -O "${pcmac_file}.downloaded" "${pcmac_url}"
if [ $? -eq 0 ]; then 
  mv "${pcmac_file}.downloaded" "${pcmac_file}"
else
  echo "scaricamento elenco mac non riuscito, viene utilizzato il file locale ${pcmac_file}"
  rm -f "${pcmac_file}.downloaded"
fi

# Filtraggio dei soli MAC delle macchine appartenenti al laboratorio
# NOTA che il file scaricato DEVE essere ordinato per codice pc in modo da far funzionare questo
macchine=( $(echo ${macchine} | tr -d ' ' | sed 's/,/\n/g' | sort) ) # Elenco dei client uno per riga, eliminando la virgola ed eventuali spazi (dentro un array)
printf '' > "${pcmac_file}.tmp"
let i=0
while read pc mac; do
  if [ "$pc" = "${macchine[$i]}" ]; then
    echo "${pc}" "${mac}" >> "${pcmac_file}.tmp"
    let i++
  fi
done < "${pcmac_file}"
# Controllo se tutti i pc sono stati trovati nel file. Nel caso, il primo che non e` stato trovato e` stampato
if [ $i -ne ${#macchine[@]} ]; then
  rm "${pcmac_file}.tmp"
  echo "indirizzo MAC per il PC '${macchine[$i]}' non trovato nel file '${pcmac_file}'"
  echo "Controllare la lista delle macchine nel file delle variabili di configurazione del laboratorio \"${config_file}\"."
  echo "Se la lista e' corretta, aggiornare il database su linux@polito in modo che contenga tutti i pc disponibili."
  exit 1
fi
# Sovrascrivo il file con tutti i pc, rimpiazzandolo con uno che contiene solo i pc del lab
mv "${pcmac_file}.tmp" "${pcmac_file}"

################################################################################
################### Server DHCP (dnsmasq) dell'ambiente LTSP ###################
################################################################################

/etc/init.d/dnsmasq stop # Ferma servzio dnsmasq

cat > "${ltsp_dnsmasq_conf}" << EOF
# Configures dnsmasq for PXE client booting.
# All the files in /etc/dnsmasq.d/ override the main dnsmasq configuration in
# /etc/dnsmasq.conf.
# You may modify this file to suit your needs, or create new ones in dnsmasq.d/.

# Log lots of extra information about DHCP transactions.
#log-dhcp

# IP ranges to hand out.
dhcp-range=${dhcp_range_begin},${dhcp_range_end},8h

# If another DHCP server is present on the network, you may use a proxy range
# instead. This makes dnsmasq provide boot information but not IP leases.
# (needs dnsmasq 2.48+)
#dhcp-range=10.0.2.2,proxy

# The rootpath option is used by both NFS and NBD.
dhcp-option=17,/opt/ltsp/i386

# Define common netboot types.
dhcp-vendorclass=etherboot,Etherboot
dhcp-vendorclass=pxe,PXEClient
dhcp-vendorclass=ltsp,"Linux ipconfig"

# Set the boot filename depending on the client vendor identifier.
# The boot filename is relative to tftp-root.
dhcp-boot=net:pxe,/ltsp/i386/pxelinux.0
dhcp-boot=net:etherboot,/ltsp/i386/nbi.img
dhcp-boot=net:ltsp,/ltsp/i386/lts.conf

# Kill multicast.
dhcp-option=vendor:pxe,6,2b

# Disable re-use of the DHCP servername and filename fields as extra
# option space. That's to avoid confusing some old or broken DHCP clients.
dhcp-no-override

# We don't want a PXE menu since we're using a graphical PXELinux menu.
#pxe-prompt="Press F8 for boot menu", 3

# The known types are x86PC, PC98, IA64_EFI, Alpha, Arc_x86,
# Intel_Lean_Client, IA32_EFI, BC_EFI, Xscale_EFI and X86-64_EFI
pxe-service=X86PC, "Boot from network", /ltsp/i386/pxelinux

# A boot service type of 0 is special, and will abort the
# net boot procedure and continue booting from local media.
#pxe-service=X86PC, "Boot from local hard disk", 0

# Comment the following to disable the TFTP server functionality of dnsmasq.
enable-tftp

# The TFTP directory. Sometimes /srv/tftp is used instead.
tftp-root=/var/lib/tftpboot/

# Disable the DNS server functionality of dnsmasq by setting port=0
port=0

# Specify per host parameters for the DHCP server
EOF

segmento_rete=$(echo -n "${network_lab}" | sed -r 's/\.[0-9]+$//')
segmento_client=$(echo -n "${dhcp_range_begin}" | sed -r 's/^([0-9]+\.){3}//')

# Per ogni client si aggiunge la configurazione DHCP specifica
ipcounter=0
while read pc mac; do
  echo "dhcp-host=${mac},${pc},${segmento_rete}.$((${segmento_client}+${ipcounter})),infinite" >> "${ltsp_dnsmasq_conf}"
  ipcounter=$((${ipcounter}+1))
done < "${pcmac_file}"

# Per ogni eventuale client X si aggiunge la configurazione DHCP specifica
xcount=1
for mac in $(echo "${XMAC}" | tr ',' ' '); do
  echo "dhcp-host=${mac},X$(printf "%03d" ${xcount}),${segmento_rete}.$((${segmento_client}+${ipcounter})),infinite" >> "${ltsp_dnsmasq_conf}"
  ipcounter=$((${ipcounter}+1))
  xcount=$((${xcount}+1))
done

> "${ltsp_dnsmasq_leases}" # Cancellazione di eventuali lease già dati dal server DHCP

/etc/init.d/dnsmasq start # Avvia servizio dnsmasq

################################################################################
########################## Creazione utente 'docente' ##########################
################################################################################

# Creazione gruppo docente dal nome della scuola
gruppo_docente=$(echo -n ${nome_lab} | tr -cd '[:alpha:]' | tr '[:upper:]' '[:lower:]')
groupadd "${gruppo_docente}"
# Creazione gruppo dei client
groupadd "${username_client_base}"

# Creazione utente 'docente'
prettyusername=$(echo ${username_docente}| sed -r 's/^(.)/\u\1/') # Rendere maiuscola la prima lettera del nome utente per abbellire informazioni finger
adduser --gecos "${prettyusername}" --disabled-login --ingroup "${gruppo_docente}" "${username_docente}"
echo "${username_docente}:${username_docente}" | chpasswd # Imposta password dell'utente 'docente'
adduser "${username_docente}" "${username_client_base}" # Aggiunta del docente anche al gruppo dei client
adduser "${username_docente}" "epoptes" # Aggiunta del docente anche al gruppo epoptes
adduser "${username_docente}" "lpadmin" # Aggiunta del docente anche al gruppo per la gestione stampanti

# Creazione della cartella 'Materiale didattico'
mkdir -p "/home/${username_docente}/Materiale didattico"
chmod 755 "/home/${username_docente}/Materiale didattico"
chown "${username_docente}:${gruppo_docente}" "/home/${username_docente}/Materiale didattico"
# Creazione della cartella 'Scrivania'
mkdir -p "/home/${username_docente}/Scrivania"
chmod 755 "/home/${username_docente}/Scrivania"
chown "root:root" "/home/${username_docente}/Scrivania" # Non permette il salvataggio di file sul desktop
# Creazione sul desktop del docente della cartella 'Documenti studenti' dei file condivisi degli studenti
mkdir -p "/home/${username_docente}/Scrivania/Documenti studenti"
chmod 755 "/home/${username_docente}/Scrivania/Documenti studenti"
chown "${username_docente}:${gruppo_docente}" "/home/${username_docente}/Scrivania/Documenti studenti"
# Creazione della cartella 'Documenti' del docente
mkdir -p "/home/${username_docente}/Documenti"
chmod 755 "/home/${username_docente}/Documenti"
chown "${username_docente}:${gruppo_docente}" "/home/${username_docente}/Documenti"
# Creazione della cartella 'Documenti condivisi' del docente
mkdir -p "/home/${username_docente}/Documenti condivisi"
chmod 777 "/home/${username_docente}/Documenti condivisi"
chown "${username_docente}" "/home/${username_docente}/Documenti condivisi" # Gruppo assegnato in seguito
# Creazione della cartella 'Modelli'
mkdir -p "/home/${username_docente}/Modelli"
chmod 775 "/home/${username_docente}/Modelli"
chown "${username_docente}:${gruppo_docente}" "/home/${username_docente}/Modelli"
cp -R "${libreoffice_dir}/." "/home/${username_docente}/Modelli" # Aggiunta dei modelli
# Creazione della cartella '.config'
mkdir -p "/home/${username_docente}/.config"
chmod 755 "/home/${username_docente}/.config"
chown "${username_docente}:${gruppo_docente}" "/home/${username_docente}/.config"

# Conferimento al docente dei permessi sulla sua home
chmod 755 "/home/${username_docente}/"
chown "${username_docente}:${gruppo_docente}" "/home/${username_docente}"

# Modifica delle ACL sui Documenti condivisi
chgrp "${username_client_base}" "/home/${username_docente}/Documenti condivisi"
chmod g+w "/home/${username_docente}/Documenti condivisi"
setfacl -d -m u::rwx,g::rwx,o::rwx "/home/${username_docente}/Documenti condivisi"

# Aggiunta del link ai file pubblici del docente sul desktop del docente
ln -Ts "/home/${username_docente}/Materiale didattico" "/home/${username_docente}/Scrivania/Materiale didattico"
# Aggiunta del link ai documenti del docente sul desktop del docente
ln -Ts "/home/${username_docente}/Documenti" "/home/${username_docente}/Scrivania/Documenti"
# Aggiunta del link ai documenti condivisi sul desktop del docente
ln -Ts "/home/${username_docente}/Documenti condivisi" "/home/${username_docente}/Scrivania/Documenti condivisi"

# Creazione del lanciatore sul desktop del docente per Firefox
cp "/usr/share/applications/firefox.desktop" "/home/${username_docente}/Scrivania/firefox.desktop"
chmod 755 "/home/${username_docente}/Scrivania/firefox.desktop"

# Creazione del lanciatore sul desktop del docente per Google Chrome
cp "/usr/share/applications/google-chrome.desktop" "/home/${username_docente}/Scrivania/google-chrome.desktop"
chmod 755 "/home/${username_docente}/Scrivania/google-chrome.desktop"

# Creazione del lanciatore sul desktop del docente per LibreOffice
cp "/usr/share/applications/libreoffice-startcenter.desktop" "/home/${username_docente}/Scrivania/libreoffice-startcenter.desktop"
sed -i '/NotShowIn/d' "/home/${username_docente}/Scrivania/libreoffice-startcenter.desktop"
chmod 755 "/home/${username_docente}/Scrivania/libreoffice-startcenter.desktop"

# Creazione del lanciatore sul desktop del docente per Scratch3
cp "/usr/share/applications/scratch-desktop.desktop" "/home/${username_docente}/Scrivania/scratch-desktop.desktop"
chmod 755 "/home/${username_docente}/Scrivania/scratch-desktop.desktop"

# Creazione del lanciatore sul desktop del docente per Epoptes
cp "/usr/share/applications/epoptes.desktop" "/home/${username_docente}/Scrivania/epoptes.desktop"
echo "Name[it]=Gestione laboratorio" >> "/home/${username_docente}/Scrivania/epoptes.desktop"
chmod 755 "/home/${username_docente}/Scrivania/epoptes.desktop"

# Creazione del lanciatore sul desktop del docente per lo spegnimento del server
cat > "/home/${username_docente}/Scrivania/poweroff.desktop" << EOF
[Desktop Entry]
Type=Application
Terminal=false
Name=Spegni PC
Comment=Spegni il computer
Exec=/usr/share/epoptes-client/endsession --shutdown
Icon=system-shutdown
EOF
chmod 755 "/home/${username_docente}/Scrivania/poweroff.desktop"

# Ordinamento icone ad ogni avvio della sessione
mkdir -p "/home/${username_docente}/.config/autostart"
chmod 755 "/home/${username_docente}/.config/autostart"
cat > "/home/${username_docente}/.config/autostart/desktop_icon_position.desktop" << EOF
[Desktop Entry]
Type=Application
Name=Desktop Icon Position
Exec=${script_dir}/desktop_icon_position.sh
EOF
chmod 755 "/home/${username_docente}/.config/autostart/desktop_icon_position.desktop"

# Reset configurazione epoptes ad ogni avvio della sessione
cat > "/home/${username_docente}/.config/autostart/epoptes_reset_config.desktop" << EOF
[Desktop Entry]
Type=Application
Name=Epoptes reset config
Exec=${script_dir}/epoptes_reset_config.sh
EOF
chmod 755 "/home/${username_docente}/.config/autostart/epoptes_reset_config.desktop"


################################################################################
######################## Creazione utenti 'studenteXX' #########################
################################################################################

for i in $(seq ${n_utenti}); do
  # Creazione utente 'studenteXX'
  username_client="${username_client_base}$(printf "%02d" ${i})"
  adduser --gecos "${username_client}" --disabled-login --ingroup "${username_client_base}" "${username_client}"
  echo "${username_client}:${username_client}" | chpasswd # Imposta password dell'utente 'studenteXX'

  # Creazione della cartella 'Scrivania'
  mkdir -p "/home/${username_client}/Scrivania"
  chmod 755 "/home/${username_client}/Scrivania"
  chown "root:root" "/home/${username_client}/Scrivania" # Non permette il salvataggio di file sul desktop
  # Creazione della cartella 'Documenti' dello studente
  mkdir -p "/home/${username_client}/Documenti"
  chmod 755 "/home/${username_client}/Documenti"
  chown "${username_client}:${username_client_base}" "/home/${username_client}/Documenti"
  # Creazione della cartella 'Modelli'
  mkdir -p "/home/${username_client}/Modelli"
  chmod 755 "/home/${username_client}/Modelli"
  chown "${username_client}:${username_client_base}" "/home/${username_client}/Modelli"
  cp -R "${libreoffice_dir}/." "/home/${username_client}/Modelli" # Aggiunta dei modelli
  # Creazione della cartella '.config'
  mkdir -p "/home/${username_client}/.config"
  chmod 755 "/home/${username_client}/.config"
  chown "${username_client}:${username_client_base}" "/home/${username_client}/.config"

  # Conferimento allo studente dei permessi sulla sua home
  chmod 755 "/home/${username_client}/"
  chown "${username_client}:${username_client_base}" "/home/${username_client}"

  # Aggiunta del link ai file pubblici del docente sul desktop del docente
  ln -Ts "/home/${username_docente}/Materiale didattico" "/home/${username_client}/Scrivania/Materiale didattico"
  # Aggiunta del link ai documenti dello studente sul desktop dello studente
  ln -Ts "/home/${username_client}/Documenti" "/home/${username_client}/Scrivania/Documenti ${username_client}"
  # Aggiunta del link ai documenti dello studente sul desktop del docente
  ln -Ts "/home/${username_client}/Documenti" "/home/${username_docente}/Scrivania/Documenti studenti/Documenti ${username_client}"
  # Aggiunta del link ai documenti condivisi sul desktop dello studente
  ln -Ts "/home/${username_docente}/Documenti condivisi" "/home/${username_client}/Scrivania/Documenti condivisi"

  # Creazione del lanciatore sul desktop dello studente per Firefox
  cp "/usr/share/applications/firefox.desktop" "/home/${username_client}/Scrivania/firefox.desktop"
  chmod 755 "/home/${username_client}/Scrivania/firefox.desktop"

  # Creazione del lanciatore sul desktop dello studente per Google Chrome
  cp "/usr/share/applications/google-chrome.desktop" "/home/${username_client}/Scrivania/google-chrome.desktop"
  chmod 755 "/home/${username_client}/Scrivania/google-chrome.desktop"

  # Creazione del lanciatore sul desktop dello studente per LibreOffice
  cp "/usr/share/applications/libreoffice-startcenter.desktop" "/home/${username_client}/Scrivania/libreoffice-startcenter.desktop"
  sed -i '/NotShowIn/d' "/home/${username_client}/Scrivania/libreoffice-startcenter.desktop"
  chmod 755 "/home/${username_client}/Scrivania/libreoffice-startcenter.desktop"

  # Creazione del lanciatore sul desktop dello studente per Scratch3
  cp "/usr/share/applications/scratch-desktop.desktop" "/home/${username_client}/Scrivania/scratch-desktop.desktop"
  chmod 755 "/home/${username_client}/Scrivania/scratch-desktop.desktop"

  # Creazione del lanciatore sul desktop dello studente per lo spegnimento del client
  cat > "/home/${username_client}/Scrivania/poweroff.desktop" << EOF
[Desktop Entry]
Type=Application
Terminal=false
Name=Spegni PC
Comment=Spegni il computer
Exec=/usr/share/epoptes-client/endsession --shutdown
Icon=system-shutdown
EOF
  chmod 755 "/home/${username_client}/Scrivania/poweroff.desktop"

  # Ordinamento icone ad ogni avvio della sessione
  mkdir -p "/home/${username_client}/.config/autostart"
  chmod 755 "/home/${username_client}/.config/autostart"
  cat > "/home/${username_client}/.config/autostart/desktop_icon_position.desktop" << EOF
[Desktop Entry]
Type=Application
Name=Desktop Icon Position
Exec=${script_dir}/desktop_icon_position.sh
EOF
  chmod 755 "/home/${username_client}/.config/autostart/desktop_icon_position.desktop"
done

################################################################################
###################### Configurazione dell'ambiente LTSP #######################
################################################################################

cat > "${ltsp_conf}" << EOF
# This file is served via TFTP from /var/lib/tftpboot/ltsp/<arch>/lts.conf
# when using the default NBD setup.
# If using NFS, it goes in /opt/ltsp/<arch>/etc/lts.conf.
# A list of directives is available at http://manpages.ubuntu.com/lts.conf
# or your installed lts.conf man page.
# Lines starting with '#' are comments.
# Avoid empty [Sections] as they're considered syntax errors.


# This section applies to all clients and is overriden by the other sections.
[Default]

# Used to chose the default session on the server
LDM_SESSION="mate-session"

# Don't encrypt X traffic. Gives much faster graphics at a security cost.
LDM_DIRECTX=True

# If the default 16bit color mode is giving you problems on thin clients:
X_SMART_COLOR_DEPTH=False

# Automatically login clients with the specified LDM_USERNAME/LDM_PASSWORD.
LDM_AUTOLOGIN=True

# Syslog configuration
LDM_SYSLOG=True
SYSLOG_HOST=${address_lab}

EOF

for i in $(seq 0 $((${n_utenti}-1))); do
  username_client="${username_client_base}$(printf "%02d" $((${i}+1)))"
  cat >> "${ltsp_conf}" << EOF

[${segmento_rete}.$((${segmento_client}+${i}))]
LDM_USERNAME=${username_client}
LDM_PASSWORD=${username_client}

EOF
done

################################################################################
########################### Configurazione syslog-ng ###########################
################################################################################

cat > "${ltsp_syslog_conf}" << EOF
source ltsp_net_udp { udp(); };
destination ltsp_hosts { file('/var/log/HOSTS/\${HOST}/\${FACILITY}.log' owner(root) group(root) perm(0600) dir_perm(0700) create_dirs(yes)); };
log { source(ltsp_net_udp); destination(ltsp_hosts); };
EOF

/etc/init.d/syslog-ng restart # Riavvio di syslog-ng per rendere effettive le modifiche

################################################################################
######################### Creazione gruppo in Epoptes ##########################
################################################################################

# Cartella contenente file configurazione gruppi su Epoptes
mkdir -p "/home/${username_docente}/.config/epoptes"
chown "${username_docente}:${gruppo_docente}" "/home/${username_docente}/.config/epoptes"

# File di configurazione del gruppo di Epoptes
epoptes_json="/home/${username_docente}/.config/epoptes/groups.json"

echo -e "{\n\t\"clients\": {" > "${epoptes_json}"

client_counter=0
while read pc mac; do
  echo -e "\t\t\"${client_counter}\": {\n\t\t\t\"alias\": \"${pc}\",\n\t\t\t\"mac\": \"${mac}\"\n\t\t}," >> "${epoptes_json}"
  client_counter=$((${client_counter}+1))
done < "${pcmac_file}"

x_counter=1
for mac in $(echo "${XMAC}" | tr ',' ' '); do # Inserimento dei client X (se ${XMAC} e' stata settata) nel file json
  echo -e "\t\t\"${client_counter}\": {\n\t\t\t\"alias\": \"X$(printf "%03d" ${x_counter})\",\n\t\t\t\"mac\": \"${mac}\"\n\t\t}," >> "${epoptes_json}"
  client_counter=$((${client_counter}+1))
  x_counter=$((${x_counter}+1))
done

sed -i "$ s/,//" "${epoptes_json}" # Eliminazione virgola dall'ultimo client inserito

echo -e "\t},\n\t\"groups\": [\n\t\t{\n\t\t\t\"name\": \"${nome_lab}\",\n\t\t\t\"members\": {" >> "${epoptes_json}"

for i in $(seq 0 $((${client_counter}-1))); do # Creazione gruppo del laboratorio nel file json
  echo -e "\t\t\t\t\"${i}\": {}," >> "${epoptes_json}"
done
sed -i "$ s/,//" "${epoptes_json}" # Eliminazione virgola dall'ultimo client inserito

echo -e "\t\t\t}\n\t\t}\n\t]\n}" >> "${epoptes_json}"

################################################################################
############################ Configurazione lightdm ############################
################################################################################

# Abilita il login manuale e configura la sessione di default in MATE Desktop
cat > "${lightdm_conf}" << EOF
[SeatDefaults]
greeter-show-manual-login=true
user-session=mate
EOF

# Setta il colore di sfondo della schermata di login
cat > "${slick_greeter_conf}" << EOF
[Greeter]
background=#0064B7
background-color=#0064B7
EOF

################################################################################
########################### Configurazione sessioni ############################
################################################################################

for file in ${xsession_dir}/* ${wayland_dir}/*; do
  if ! [ "${file}" = "${default_session_desktop}" ]; then
    if ! [[ $file = *".bak"* ]]; then
      mv "${file}" "${file}.bak"
    fi
  fi
done

################################################################################
######################## Configurazione AccountsService ########################
################################################################################

# Nascondere l'account di amministrazione dall'elenco degli utenti nella schermata di login
sed -i "s/SystemAccount=false/SystemAccount=true/" "${accounts_service_dir}/${username_admin}"

# Nascondere gli account studenteXX dall'elenco degli utenti nella schermata di login
for i in $(seq 0 $((${n_utenti}-1))); do
  username_client="${username_client_base}$(printf "%02d" $((${i}+1)))"
  cat > "${accounts_service_dir}/${username_client}" << EOF
[User]
SystemAccount=true
EOF
done

################################################################################
############################# Configurazione Polkit ############################
################################################################################

# Creazione cartella di configurazione di Polkit
mkdir -p "${polkit_dir}"

# Disabilita montaggio di un file-system da un dispositivo collegato in un'altra posizione
cat > "${polkit_conf}" << EOF
polkit.addRule(function(action, subject) {
    if (action.id == "org.freedesktop.udisks2.filesystem-mount-other-seat") {
        return polkit.Result.NO;
    }
});
EOF

################################################################################
########################### Configurazione nm-applet ###########################
################################################################################

mv "${nm_applet_conf}" "${nm_applet_conf}.bak"

################################################################################
############################# Configurazione CUPS ##############################
################################################################################

mv "${cups_server_conf}" "${cups_server_conf}".bak
cat > "${cups_server_conf}" << EOF
LogLevel warn
PageLogFormat
MaxLogSize 0
Listen localhost:631
Listen /run/cups/cups.sock
Browsing Off
BrowseLocalProtocols dnssd
DefaultAuthType Basic
WebInterface Yes
<Location />
  Order allow,deny
</Location>
<Location /admin>
  Order allow,deny
</Location>
<Location /admin/conf>
  AuthType Default
  Require user @SYSTEM
  Order allow,deny
</Location>
<Location /admin/log>
  AuthType Default
  Require user @SYSTEM
  Order allow,deny
</Location>
<Policy default>
  JobPrivateAccess default
  JobPrivateValues default
  SubscriptionPrivateAccess default
  SubscriptionPrivateValues default
  <Limit Create-Job Print-Job Print-URI Validate-Job>
    Order deny,allow
  </Limit>
  <Limit Send-Document Send-URI Hold-Job Release-Job Restart-Job Purge-Jobs Set-Job-Attributes Create-Job-Subscription Renew-Subscription Cancel-Subscription Get-Notifications Reprocess-Job Cancel-Current-Job Suspend-Current-Job Resume-Job Cancel-My-Jobs Close-Job CUPS-Move-Job CUPS-Get-Document>
    Require user @OWNER @SYSTEM
    Order deny,allow
  </Limit>
  <Limit CUPS-Add-Modify-Printer CUPS-Delete-Printer CUPS-Add-Modify-Class CUPS-Delete-Class CUPS-Set-Default CUPS-Get-Devices>
    AuthType Default
    Require user @SYSTEM
    Order deny,allow
  </Limit>
  <Limit Pause-Printer Resume-Printer Enable-Printer Disable-Printer Pause-Printer-After-Current-Job Hold-New-Jobs Release-Held-New-Jobs Deactivate-Printer Activate-Printer Restart-Printer Shutdown-Printer Startup-Printer Promote-Job Schedule-Job-After Cancel-Jobs CUPS-Accept-Jobs CUPS-Reject-Jobs>
    AuthType Default
    Require user @SYSTEM
    Order deny,allow
  </Limit>
  <Limit CUPS-Authenticate-Job>
    Require user @OWNER @SYSTEM
    Order deny,allow
  </Limit>
  <Limit All>
    Order deny,allow
  </Limit>
</Policy>
<Policy authenticated>
  JobPrivateAccess default
  JobPrivateValues default
  SubscriptionPrivateAccess default
  SubscriptionPrivateValues default
  <Limit Create-Job Print-Job Print-URI Validate-Job>
    AuthType Default
    Order deny,allow
  </Limit>
  <Limit Send-Document Send-URI Hold-Job Release-Job Restart-Job Purge-Jobs Set-Job-Attributes Create-Job-Subscription Renew-Subscription Cancel-Subscription Get-Notifications Reprocess-Job Cancel-Current-Job Suspend-Current-Job Resume-Job Cancel-My-Jobs Close-Job CUPS-Move-Job CUPS-Get-Document>
    AuthType Default
    Require user @OWNER @SYSTEM
    Order deny,allow
  </Limit>
  <Limit CUPS-Add-Modify-Printer CUPS-Delete-Printer CUPS-Add-Modify-Class CUPS-Delete-Class CUPS-Set-Default>
    AuthType Default
    Require user @SYSTEM
    Order deny,allow
  </Limit>
  <Limit Pause-Printer Resume-Printer Enable-Printer Disable-Printer Pause-Printer-After-Current-Job Hold-New-Jobs Release-Held-New-Jobs Deactivate-Printer Activate-Printer Restart-Printer Shutdown-Printer Startup-Printer Promote-Job Schedule-Job-After Cancel-Jobs CUPS-Accept-Jobs CUPS-Reject-Jobs>
    AuthType Default
    Require user @SYSTEM
    Order deny,allow
  </Limit>
  <Limit Cancel-Job CUPS-Authenticate-Job>
    AuthType Default
    Require user @OWNER @SYSTEM
    Order deny,allow
  </Limit>
  <Limit All>
    Order deny,allow
  </Limit>
</Policy>
<Policy kerberos>
  JobPrivateAccess default
  JobPrivateValues default
  SubscriptionPrivateAccess default
  SubscriptionPrivateValues default
  <Limit Create-Job Print-Job Print-URI Validate-Job>
    AuthType Negotiate
    Order deny,allow
  </Limit>
  <Limit Send-Document Send-URI Hold-Job Release-Job Restart-Job Purge-Jobs Set-Job-Attributes Create-Job-Subscription Renew-Subscription Cancel-Subscription Get-Notifications Reprocess-Job Cancel-Current-Job Suspend-Current-Job Resume-Job Cancel-My-Jobs Close-Job CUPS-Move-Job CUPS-Get-Document>
    AuthType Negotiate
    Require user @OWNER @SYSTEM
    Order deny,allow
  </Limit>
  <Limit CUPS-Add-Modify-Printer CUPS-Delete-Printer CUPS-Add-Modify-Class CUPS-Delete-Class CUPS-Set-Default>
    AuthType Default
    Require user @SYSTEM
    Order deny,allow
  </Limit>
  <Limit Pause-Printer Resume-Printer Enable-Printer Disable-Printer Pause-Printer-After-Current-Job Hold-New-Jobs Release-Held-New-Jobs Deactivate-Printer Activate-Printer Restart-Printer Shutdown-Printer Startup-Printer Promote-Job Schedule-Job-After Cancel-Jobs CUPS-Accept-Jobs CUPS-Reject-Jobs>
    AuthType Default
    Require user @SYSTEM
    Order deny,allow
  </Limit>
  <Limit Cancel-Job CUPS-Authenticate-Job>
    AuthType Negotiate
    Require user @OWNER @SYSTEM
    Order deny,allow
  </Limit>
  <Limit All>
    Order deny,allow
  </Limit>
</Policy>
EOF

systemctl restart cups

################################################################################
############################# Configurazione dconf #############################
################################################################################

# Download dell'immagine di sfondo
wget -t 1 -T 20 -nv -O "/usr/share/backgrounds/liberoMareReversedOpenScuola.jpg" "http://linux.studenti.polito.it/wp/wp-content/uploads/liberoMareReversedOpenScuola.jpg"

# Creazione del profilo per dconf
cat > "${dconf_dir}/profile/user" << EOF
user-db:user
system-db:${dconf_db}
EOF

# Creazione della cartella del db relativo all'utente precedentemente creato
mkdir -p "${dconf_dir}/db/${dconf_db}.d"

# Impostazioni dconf del db precedentemente creato
cat > "${dconf_dir}/db/${dconf_db}.d/settings" << EOF
[org/mate/desktop/session]
idle-delay=10

[org/mate/screensaver]
idle-activation-enabled=true
lock-enabled=false
logout-enabled=false
user-switch-enabled=false
mode='blank-only'

[org/mate/desktop/background]
background-fade=false
color-shading-type='solid'
draw-background=true
picture-filename='/usr/share/backgrounds/liberoMareReversedOpenScuola.jpg'
picture-opacity=100
picture-options='scaled'
primary-color='#0064B7'
secondary-color='#0064B7'
show-desktop-icons=true

[org/mate/caja/desktop]
home-icon-visible=false
network-icon-visible=false
trash-icon-visible=false
volumes-visible=false

[org/mate/desktop/interface]
gtk-theme='TraditionalOk'
icon-theme='oxygen'

[org/mate/desktop/lockdown]
disable-lock-screen=true
disable-user-switching=true
disable-theme-settings=true

[apps/indicator-session]
force-restart-menuitem=false
show-real-name-on-panel=false
suppress-logout-menuitem=true
suppress-logout-restart-shutdown=true
suppress-restart-menuitem=true
suppress-shutdown-menuitem=true
user-show-menu=false

[org/mate/panel/general]
object-id-list=['briskmenu', 'notification-area', 'indicatorappletcomplete', 'clock', 'show-desktop', 'window-list', 'trashapplet']
locked-down=true
toplevel-id-list=['top', 'bottom']

[org/mate/panel/toplevels/top]
expand=true
orientation='top'
screen=0
size=28
x=0
y=0

[org/mate/panel/toplevels/bottom]
expand=true
orientation='bottom'
screen=0
size=28
x=0
y-bottom=0

[org/mate/panel/objects/briskmenu]
applet-iid='BriskMenuFactory::BriskMenu'
locked=true
toplevel-id='top'
position=0
object-type='applet'

[org/mate/panel/objects/notification-area]
applet-iid='NotificationAreaAppletFactory::NotificationArea'
locked=true
toplevel-id='top'
position=20
object-type='applet'
panel-right-stick=true

[org/mate/panel/objects/indicatorappletcomplete]
applet-iid='IndicatorAppletCompleteFactory::IndicatorAppletComplete'
locked=true
toplevel-id='top'
position=10
object-type='applet'
panel-right-stick=true

[org/mate/panel/objects/clock]
applet-iid='ClockAppletFactory::ClockApplet'
locked=true
toplevel-id='top'
position=0
object-type='applet'
panel-right-stick=true

[org/mate/panel/objects/show-desktop]
applet-iid='WnckletFactory::ShowDesktopApplet'
locked=true
toplevel-id='bottom'
position=0
object-type='applet'

[org/mate/panel/objects/window-list]
applet-iid='WnckletFactory::WindowListApplet'
locked=true
toplevel-id='bottom'
position=20
object-type='applet'

[org/mate/panel/objects/trashapplet]
applet-iid='TrashAppletFactory::TrashApplet'
locked=true
toplevel-id='bottom'
position=0
object-type='applet'
panel-right-stick=true

[org/mate/desktop/media-handling]
automount=false
automount-open=false

[org/mate/marco/general]
compositing-manager=false
EOF

# Rende effettive le modifiche effettuate precedentemente
dconf update

# Creazione cartella per bloccare impostazioni dconf
mkdir -p "${dconf_dir}/db/${dconf_db}.d/locks"

# Bloccare impostazioni dconf
cat > "${dconf_dir}/db/${dconf_db}.d/locks/settings" << EOF
/org/mate/desktop/session/idle-delay

/org/mate/screensaver/idle-activation-enabled
/org/mate/screensaver/lock-enabled
/org/mate/screensaver/logout-enabled
/org/mate/screensaver/user-switch-enabled
/org/mate/screensaver/mode

/org/mate/desktop/background/background-fade
/org/mate/desktop/background/color-shading-type
/org/mate/desktop/background/draw-background
/org/mate/desktop/background/picture-filename
/org/mate/desktop/background/picture-opacity
/org/mate/desktop/background/picture-options
/org/mate/desktop/background/primary-color
/org/mate/desktop/background/secondary-color
/org/mate/desktop/background/show-desktop-icons

/org/mate/caja/desktop/home-icon-visible
/org/mate/caja/desktop/network-icon-visible
/org/mate/caja/desktop/trash-icon-visible
/org/mate/caja/desktop/volumes-visible

/org/mate/desktop/interface/gtk-theme
/org/mate/desktop/interface/icon-theme

/org/mate/desktop/lockdown/disable-lock-screen
/org/mate/desktop/lockdown/disable-user-switching
/org/mate/desktop/lockdown/disable-theme-settings

/apps/indicator-session/force-restart-menuitem
/apps/indicator-session/show-real-name-on-panel
/apps/indicator-session/suppress-logout-menuitem
/apps/indicator-session/suppress-logout-restart-shutdown
/apps/indicator-session/suppress-restart-menuitem
/apps/indicator-session/suppress-shutdown-menuitem
/apps/indicator-session/user-show-menu

/org/mate/panel/general/object-id-list
/org/mate/panel/general/locked-down
/org/mate/panel/general/toplevel-id-list

/org/mate/panel/toplevels/top/expand
/org/mate/panel/toplevels/top/orientation
/org/mate/panel/toplevels/top/screen
/org/mate/panel/toplevels/top/size
/org/mate/panel/toplevels/top/x
/org/mate/panel/toplevels/top/y

/org/mate/panel/toplevels/bottom/expand
/org/mate/panel/toplevels/bottom/orientation
/org/mate/panel/toplevels/bottom/screen
/org/mate/panel/toplevels/bottom/size
/org/mate/panel/toplevels/bottom/x
/org/mate/panel/toplevels/bottom/y-bottom

/org/mate/panel/objects/briskmenu/applet-iid
/org/mate/panel/objects/briskmenu/locked
/org/mate/panel/objects/briskmenu/toplevel-id
/org/mate/panel/objects/briskmenu/position
/org/mate/panel/objects/briskmenu/object-type

/org/mate/panel/objects/notification-area/applet-iid
/org/mate/panel/objects/notification-area/locked
/org/mate/panel/objects/notification-area/toplevel-id
/org/mate/panel/objects/notification-area/position
/org/mate/panel/objects/notification-area/object-type
/org/mate/panel/objects/notification-area/panel-right-stick

/org/mate/panel/objects/indicatorappletcomplete/applet-iid
/org/mate/panel/objects/indicatorappletcomplete/locked
/org/mate/panel/objects/indicatorappletcomplete/toplevel-id
/org/mate/panel/objects/indicatorappletcomplete/position
/org/mate/panel/objects/indicatorappletcomplete/object-type
/org/mate/panel/objects/indicatorappletcomplete/panel-right-stick

/org/mate/panel/objects/clock/applet-iid
/org/mate/panel/objects/clock/locked
/org/mate/panel/objects/clock/toplevel-id
/org/mate/panel/objects/clock/position
/org/mate/panel/objects/clock/object-type
/org/mate/panel/objects/clock/panel-right-stick

/org/mate/panel/objects/show-desktop/applet-iid
/org/mate/panel/objects/show-desktop/locked
/org/mate/panel/objects/show-desktop/toplevel-id
/org/mate/panel/objects/show-desktop/position
/org/mate/panel/objects/show-desktop/object-type

/org/mate/panel/objects/window-list/applet-iid
/org/mate/panel/objects/window-list/locked
/org/mate/panel/objects/window-list/toplevel-id
/org/mate/panel/objects/window-list/position
/org/mate/panel/objects/window-list/object-type

/org/mate/panel/objects/trashapplet/applet-iid
/org/mate/panel/objects/trashapplet/locked
/org/mate/panel/objects/trashapplet/toplevel-id
/org/mate/panel/objects/trashapplet/position
/org/mate/panel/objects/trashapplet/object-type
/org/mate/panel/objects/trashapplet/panel-right-stick

/org/mate/desktop/media-handling/automount
/org/mate/desktop/media-handling/automount-open

/org/mate/marco/general/compositing-manager
EOF

# Rende effettive le modifiche effettuate precedentemente
dconf update

################################################################################
############################## Configurazione VNC ##############################
################################################################################

exit_code=1
while [ ${exit_code} -ne 0 ]; do
  x11vnc -storepasswd ${vnc_pw_conf}
  exit_code=${?}
done

cat > "${vnc_conf}" << EOF
[Unit]
Description=Start x11vnc at startup.
After=multi-user.target

[Service]
Type=simple
ExecStart=/usr/bin/x11vnc -loop -xkb -noxrecord -noxfixes -noxdamage -forever -auth /var/run/lightdm/root/:0 -rfbauth /etc/x11vnc.pass -rfbport 5900 -o /var/log/x11vnc.log

[Install]
WantedBy=multi-user.target
EOF

systemctl enable x11vnc.service
systemctl daemon-reload
systemctl start x11vnc.service


################################################################################
############################## Configurazione sysctl ###########################
################################################################################

cat > "${sysctl_conf}" << EOF
# Abilita il forwarding di pacchetti IPv4
net.ipv4.ip_forward=1
EOF

################################################################################
############################## Openscuola password #############################
################################################################################

exit_code=1
while [ ${exit_code} -ne 0 ]; do
  echo "Inserire la password dell'utente ${username_admin}"
  passwd ${username_admin}
  exit_code=${?}
done

################################################################################
############################## Hostname ########################################
################################################################################

sed -Ei "s/(SRV-).+(-OSC)/\1${nome_lab}\2/" "/etc/hostname"
sed -Ei "s/(SRV-).+(-OSC)/\1${nome_lab}\2/" "/etc/hosts"

################################################################################
############################# Riavvio del sistema ##############################
################################################################################

echo -e "\nE' necessario riavviare il sistema per rendere effettive le modifiche."
read -r -p "Eseguire il riavvio? [S/n]: " risposta
case "${risposta}" in
  [nN][oO]|[nN])
    echo "Eseguire il riavvio manualmente!"
    ;;

  *)
    reboot
    ;;
esac
