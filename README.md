# Script laboratori OpenScuola
Script di creazione laboratori e aggiornamento OpenScuola.

Questo repository contiene gli script.

I modelli Libreoffice sono contenuti in un [altro](https://gitlab.com/openscuola/modelli-libreoffice) repository.

## Branch
Questo repository ha due filoni principali, uno adatto alla 18.04, l'altro per la 22.04.

La versione 18.04 usa due branch:
- il ramo `18_04_develop` contiene tutti i commit, utile in fase di sviluppo e potenzialmente instabile
- il ramo `18_04`, invece, è quello da usare in produzione. Periodicamente si estraggono le ultime modifiche dal ramo develop, non appena sono stabilizzate.

Lo stesso vale per la versione 22.04, con i rami:
- `master`, Tutti i commit più recenti e per l'ultima versione
- `22_04`, è quello da usare in produzione

Il branch 20_04 rimane per retrocompatibiltà

I computer di laboratorio devono essere puntati ai branch di produzione

# Configurazione
## Primo scaricamento
```
sudo -i
apt install git
mkdir /opt/openscuola
cd /opt/openscuola
git clone -b 22_04 https://gitlab.com/openscuola/lab-configuration-scripts.git
cd lab-configuration-scripts
exit
```

Se invece di desiderasse scaricare la versione precedente, indicare il branch corretto. Per esempio, per la 18.04
```
git clone -b 18_04 https://gitlab.com/openscuola/lab-configuration-scripts.git
```

Di qui, si può trovare il file 00preinit.sh per inizializzare il sistema openScuola.
Il file labconfig.sh modificabile viene posto in /opt/openscuola

# Copyright and License
Copyright © 2014,2015,2016,2017,2018,2019,2020,2021,2022 Alessandro Ugo, Marco Sciuto, Filippo Bonazzi, Marco De Benedictis, Carmelo Riolo, Giuseppe D'Andrea, Massimo Gismondi, Paolo Dimasi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>
