#!/bin/bash

# Copyright © 2014,2015,2016,2017,2018,2019,2020,2021 Alessandro Ugo, Marco Sciuto, Filippo Bonazzi, Marco De Benedictis, Carmelo Riolo, Giuseppe D'Andrea, Massimo Gismondi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>

# Script per il posizionamento orizzontale delle icone sul desktop

x_position=64
y_position=26
x_resolution="$(xrandr | grep '*' | head -n 1 | cut -d x -f 1 | tr -dC '[:digit:]')"
# default a 1024 pixel se non si è potuto prendere la risoluzione
x_resolution=${x_resolution:-1024}
ls -1X "${HOME}/Scrivania/" | while read desktop_file; do
  if [ $((${x_position}+90)) -gt ${x_resolution} ]; then
    let y_position+=100
    x_position=64
  fi
  gio set "${HOME}/Scrivania/${desktop_file}" metadata::caja-icon-position ${x_position},${y_position}
  let x_position+=156
done
