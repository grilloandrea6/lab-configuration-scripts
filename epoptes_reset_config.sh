#!/bin/sh

# Copyright © 2014,2015,2016,2017,2018,2019,2020,2021 Alessandro Ugo, Marco Sciuto, Filippo Bonazzi, Marco De Benedictis, Carmelo Riolo, Giuseppe D'Andrea
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Reimposta la configurazione di epoptes
# in particolare, imposta la visualizzazione etichette "Computer (utente)"
if [ ! -d "${HOME}/.config/epoptes" ]; then
  mkdir -p "${HOME}/.config/epoptes"
fi

cd "${HOME}/.config/epoptes"
if [ ! -f settings ]; then
  cat > settings << EOF
[GUI]
label = rmi_labels_host_user
EOF
else
  sed -i -E 's/^label = .*$/label = rmi_labels_host_user/' settings
fi