#!/bin/bash
#
# Copyright © 2014,2015,2016,2017,2018,2019,2020,2021 Alessandro Ugo, Marco Sciuto, Filippo Bonazzi, Marco De Benedictis, Carmelo Riolo, Giuseppe D'Andrea, Massimo Gismondi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>
################################################################################
################# VARIABILI DI CONFIGURAZIONE DEL LABORATORIO ##################
################################################################################

# Nome della scuola
# NON inserire caratteri speciali [àèéìòù]
nome_lab="Scuola1"

# Codice laboratorio [10] utilizzato per lo spazio di indirizzamento
# Numeri dispari per laboratori successivi al primo nella stessa scuola
codice_lab="10"

# Nome utente dell'account di amministrazione [openscuola]
username_admin="openscuola"

# Nome utente base dei client [studente] utilizzato per generare sequenzialmente gli utenti [studenteXX]
username_client_base="studente"

# Nome utente per l'account docente [docente]
username_docente="docente"

# Codici identificativi delle macchine del laboratorio, separati da virgole [IA001,IA002,...].
# NON inserire il server e i client X
macchine="IA001,IA002"

# MAC address dei client X della scuola, separati da virgole [MAC1,MAC2,...].
# NON riempire la variabile nel caso non siano presenti client X.
XMAC=""

# Numero degli utenti da creare [12]
n_utenti="12"

# Parametri della rete del laboratorio che è ha il formato 10.100.y.0/24, dove y è il codice della scuola (${codice_lab}).
  # MAC dell'interfaccia di rete del laboratorio
  # Nel caso di più interfacce di rete in parallelo, specificare i MAC separati da virgole
  #  (esempio: 11:11:11:11:11:11,22:22:22:22:22:22,...)
  iface_lab_mac="XX:XX:XX:XX:XX:XX"
  # Network della rete del laboratorio [10.100.y.0]
  network_lab="10.100.${codice_lab}.0"
  # IP del server LTSP [10.100.y.10]
  address_lab="10.100.${codice_lab}.10"
  # Netmask [/24]
  netmask_lab="/24"
  # IP iniziale dei client [10.100.y.21]
  dhcp_range_begin="10.100.${codice_lab}.21"
  # IP finale dei client [10.100.y.50]
  dhcp_range_end="10.100.${codice_lab}.$((21+${n_utenti}-1))"

# Parametri della rete esterna (specifici per ogni scuola)
  # Tipo di rete [static/DHCP] (Qualsiasi valore diverso da DHCP viene interpretato come statico)
  ip_type_out="DHCP"
  # MAC dell'interfaccia di rete esterna
  iface_out_mac="XX:XX:XX:XX:XX:XX"
  # IP del server nella rete esterna (Ignorato per configurazione DHCP)
  address_out=""
  # Netmask [/24] (Ignorato per configurazione DHCP)
  netmask_out="/24"
  # Gateway (Ignorato per configurazione DHCP)
  gateway_out=""
  # Server DNS da utilizzare per la rete esterna [x.x.x.x, x.x.x.x, ...] (Sia per static che per DHCP)
  # I primi due DNS elencati [208.67.222.123, 208.67.220.123] sono di OpenDNS con Family Shield per bloccare i siti per adulti. NON ELIMINARLI!
  # I DNS elencati avranno sempre precedenza sugli eventuali DNS ricevuti tramite DHCP.
  dns_nameservers_out="208.67.222.123, 208.67.220.123, 8.8.8.8, 8.8.4.4"
