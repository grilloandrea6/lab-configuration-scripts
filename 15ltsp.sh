#!/bin/bash

# Copyright © 2014,2015,2016,2017,2018,2019,2020,2021 Alessandro Ugo, Marco Sciuto, Filippo Bonazzi, Marco De Benedictis, Carmelo Riolo, Giuseppe D'Andrea, Massimo Gismondi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>

################################################################################
######################## Controllo dei permessi di root ########################
################################################################################

if ! [ $(id -u) = 0 ]; then
  echo "Sono necessari i permessi di root!"
  exit 1
fi

################################################################################
######################## Inizializzazione ambiente LTSP ########################
################################################################################

# Per evitare alcuni problemi in Ubuntu MATE
apt purge --auto-remove mate-hud snapd
apt install synaptic

# Repository Greek schools PPA. È mantenuto dagli sviluppatori di LTSP e offre release di LSTP che sono molto più stabili di quelle presenti nei repository Ubuntu
add-apt-repository ppa:ts.sch.gr
apt update

# Installazione di LTSP in modalità "chroot"
apt install ltsp-server-standalone epoptes
gpasswd -a openscuola epoptes
ltsp-build-client --arch i386 --purge-chroot --mount-package-cache --extra-mirror 'http://ppa.launchpad.net/ts.sch.gr/ppa/ubuntu bionic main' --apt-keys '/etc/apt/trusted.gpg.d/ts_sch_gr_ubuntu_ppa.gpg' --late-packages epoptes-client

# Generazione file /etc/dnsmasq.d/ltsp-server-dnsmasq.conf
ltsp-config dnsmasq --no-proxy-dhcp

# Impostazioni per server DHCP
sed -i 's/^#[ ]*IPAPPEND=[0-9]$/IPAPPEND=2/' "/opt/ltsp/i386/etc/ltsp/update-kernels.conf"
ltsp-chroot /usr/share/ltsp/update-kernels
ltsp-update-kernels # Aggiorna il file /var/lib/tftpboot/ltsp/i386/pxelinux.cfg/default

# Generazione file /var/lib/tftpboot/ltsp/i386/lts.conf
ltsp-config lts.conf
